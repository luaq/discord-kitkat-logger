import { Client, User } from "discord.js";
import { exec } from "child_process";
import * as path from "path";
import * as fs from "fs";

// where you place your token
import { token } from "./token.json";


// options

const logFile = "messages.log";
const disableDiscordClient = true;

// end options

// logging system

type MessageAction = "CREATE" | "EDIT" | "DELETE";
interface MessageDetails {
  author?: User | null;
  id?: string;

  message?: string | null;
  lastMessage?: string | null;
};

class MessageEvent {
  readonly action: MessageAction;
  readonly details: MessageDetails;

  constructor(action: MessageAction, details: MessageDetails) {
    this.action = action;
    this.details = details;
  }
}

const client = new Client();
const botSentMessages: string[] = [];

const fullLogPath = path.join(process.cwd(), logFile);

client.once("ready", () => {
  console.log(`Ready to start logging messages and edits.`);
  console.log(`Using the Discord account ${client.user?.tag}`);

  // set the discord to invisible
  client.user?.setPresence({ status: "invisible" }).then(() => {
    // check the status was actually updated
    if (client.user?.presence.status !== "offline") {
      return;
    }

    console.log("Status successfully set to invisible.");
  });

  // check if the user wants their client killed
  if (!disableDiscordClient) {
    return;
  }

  console.log("Starting Discord assassin service...");
  setInterval(discordTask, 1000);
});

client.on("message", msg => {
  if (msg.author.bot || msg.channel.type !== "dm") {
    return; // idk, let's just not log bot messages or non-dms
  }

  // if the message author is us then delete
  // completely prohibit use of Discord
  if (msg.author.id === client.user?.id && disableDiscordClient && botSentMessages.includes(msg.id)) {
    msg.delete();
    return;
  }

  // log the message
  logMessage(new MessageEvent("CREATE", {
    author: msg.author,
    id: msg.id,
    message: msg.content
  }));
});

client.on("messageUpdate", (msg, updatedMsg) => {
  if (msg.author?.bot || msg.channel.type !== "dm") {
    return; // idk, let's just not log bot messages or non-dms
  }

  logMessage(new MessageEvent("EDIT", {
    id: updatedMsg.id,
    author: updatedMsg.author,

    lastMessage: msg.content,
    message: updatedMsg.content
  }))
});

client.on("messageDelete", msg => {
  if (msg.author?.bot || msg.channel.type !== "dm") {
    return; // idk, let's just not log bot messages or non-dms
  }

  // log the deletion of the message
  logMessage(new MessageEvent("DELETE", {
    author: msg.author,
    id: msg.id,
    message: msg.content
  }));
});

function discordTask() {
  exec("tasklist", (err, stdout) => {
    // no errors, that's bad
    if (err) {
      console.log("Failed to get the tasklist.");
      return;
    }
    
    const lines = stdout.split("\n");
    const spaceSplitter = /\s+/g;
    let discordPids = [];
    
    // go through each line of the output
    for (let line of lines) {
      const parts = line.split(spaceSplitter);
      const taskName = parts[0].toLowerCase();
      const pid = parts[1];

      if (!taskName.includes("discord")) {
        continue; // this is not discord
      }

      // add the id to the list of them
      discordPids.push(pid);
    }

    // no discord was found, good boy
    if (discordPids.length === 0) {
      return;
    }

    console.log(`Found ${discordPids.length} Discord processes, ending all...`);
    // end all discord processes
    for (let pid of discordPids) {
      // force kill the process
      exec(`taskkill /PID ${pid} /F`, err => {
        if (!err) {
          return;
        }

        // log errors with ending process
        console.error(err);
      });
    }

    // announce completion
    console.log("Finished, don't open Discord again.");
  });
}

function logMessage(event: MessageEvent) {
  // if the log files does not exist then create it
  if (!fs.existsSync(fullLogPath)) {
    fs.writeFileSync(fullLogPath, "", { encoding: "utf-8" });
  }

  const date = new Date();
  const dateFormat = `${date.getMonth()}/${date.getDay()}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
  console.log(`[${dateFormat}] Message ${event.action} event received, adding to ${logFile}`);

  // open the log file in append mode
  fs.open(fullLogPath, "a", (err, log) => {
    // if there's an error log and exit
    if (err || !log) {
      console.error(err);
      return;
    }

    // show changes for edits
    let contentDisplay = (event.action === "EDIT" && event.details.lastMessage ? `${event.details.lastMessage} =>` : "") + event.details.message;

    // append the log line
    fs.appendFileSync(log, `[${dateFormat}] [${event.action} ${event.details.id}] ${event.details.author?.tag}: ${contentDisplay}\n`, { encoding: "utf-8" });
  });
}

// end logging system

// login with my user token, log error
client.login(token);
