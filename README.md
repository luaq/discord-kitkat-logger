# Discord KitKat
In this version of Discord, you are allowed to take a **break**. Get it? Because KitKat? Haha. *This is also a message logger btw.* Created when I needed a quick one or two day break and I know I have no self-control.

## How to use?
Make a file called `token.json` in the `src` directory.
Fill in the contents:
```json
{
  "token": "YOUR USER TOKEN HERE"
}
```

## It's not working
Not my problem, just don't open Discord. ~~But if it's not working make sure Discord.JS version is < v12, they got rid of user token support, the assholes.~~

## Configuring the program
There's two options, don't know what you want to change but it's in `src/index.ts`. If you break the program, don't look at me.

## What do I do without Discord?
- Music
- Games
- Basically anything you normally do but with no friends, you'll survive you absolute introvert

## But luAq I JUst wAnt To LOg mEsSages
Then turn off the Discord assassin option in the `src/index.ts` file and restart the program.
```ts
const disableDiscordClient = false;
```
